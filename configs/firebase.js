const FirebaseREST = require('firebase-rest').default
var URL = 'https://name-project.firebaseio.com/'
var SECRET = 'SECRET'

module.exports = {
    FireBaseDB_Standard: () => {
        var standardClient =
            new FirebaseREST.Client(URL,{auth:SECRET})
        if(standardClient)
            return standardClient
        return undefined
    },
    FireBaseDB_Json:()=>{
        var jsonCLient = new FirebaseREST.JSONClient(URL,{auth:SECRET})
        if(jsonCLient)
            return jsonCLient
        return undefined
    }

}