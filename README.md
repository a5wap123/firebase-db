## db-get

`url get all`: your-domain/db-get/?token={token}<br/>
`url get for path`: your-domain/db-get/{path}?token={token}<br>

## db-set

`url set all`: your-domain/db-set/?data={data}(base64 and url encode)token={token}<br>
`url set for path`: your-domain/db-set/{path}?data={data}(base64 and url encode)&token={token}<br>

## config

`firebase`: configs/firebase.js

``` javascript
var URL = 'https://name-project.firebaseio.com/'
var SECRET = 'SECRET'
```

`token`: configs/common.js

```javascript
 TOKEN:'your-token'

```