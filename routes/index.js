var express = require('express');
var router = express.Router();
var fireBase = require('../configs/firebase').FireBaseDB_Json
const { base64encode, base64decode } = require('nodejs-base64')


/* GET home page. */
router.get('/', function(req, res, next) {
 res.send('hello api Arduino ^^')
});

router.get('/db-get/:path(*)',(req,res,next)=>{

  let path = req.params.path
  let db = fireBase()
  if(db){
    db.get(`${path}`).then(
      result =>{
        if(result.status === 200)
        { res.json( result.body)}
         else
        { res.json('error base url')}
      }
    )
  }
  else{
    res.json('error db connect')
  }
})
router.get('/db-set/:path(*)',(req,res,next)=>{
  let path = req.params.path
  let data = req.query.data
  let payload = base64decode(data)

  //parse json
  try {
    payload = JSON.parse(payload)
  } catch (error) {
    console.log(error)
  }

  let db = fireBase()
  if(db){
    db.set(`${path}`,payload).then(
      result =>{
        if(result.status === 200)
        { res.json( result.body)}
         else
        { res.json('error base url')}
      }
    )
  }
  else{
    res.json('error db connect')
  }
})
module.exports = router;
